﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompteEstBon
{
    public partial class Fin : Form
    {
        public Fin()
        {
            InitializeComponent();
        }
        public Fin(string EndText)
        {
            InitializeComponent();
            txt.Text = EndText;     //affiche le texte de fin
        }

        private void game_Click(object sender, EventArgs e)
        {
            //regénère un formulaire pour jouer puis détruit le formulaire actuel
            Form1 NewForm = new Form1();
            NewForm.Show();
            this.Dispose(false);
        }
    }
}
