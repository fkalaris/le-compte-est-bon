﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CompteEstBon
{
    class Bag
    {
        //contient tous les nombres possible dans le compte est bon
        private int[] listeNb = { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 25,25, 50,50, 75,75, 100,100 };

        //liste de nombre encore disponible a picknumber
        private List<int> numberPossible = new List<int>();

        Random rnd = new Random();
        public Bag()
        {
            //copie le tableau listeNb dans la liste number
            for (int i = 0; i < this.listeNb.Length; i++)
            {
                this.numberPossible.Add(this.listeNb[i]);
            }
        }

        //prend un des nombre dans la liste de nombre possible
        public int picknumber()
        {
            //prend un nombre random dans la list
            int rng = rnd.Next(this.numberPossible.Count);
            int pick = numberPossible[rng];

            //enleve le nombre qui à été pris de la liste
            numberPossible.Remove(rng);
            return pick;
        }
    }
}
