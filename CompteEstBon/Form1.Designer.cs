﻿namespace CompteEstBon
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.n1 = new System.Windows.Forms.Button();
            this.n2 = new System.Windows.Forms.Button();
            this.n3 = new System.Windows.Forms.Button();
            this.n4 = new System.Windows.Forms.Button();
            this.n5 = new System.Windows.Forms.Button();
            this.n6 = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.multi = new System.Windows.Forms.Button();
            this.div = new System.Windows.Forms.Button();
            this.valider = new System.Windows.Forms.Button();
            this.effacer = new System.Windows.Forms.Button();
            this.recommencer = new System.Windows.Forms.Button();
            this.resultat = new System.Windows.Forms.Label();
            this.p1 = new System.Windows.Forms.Label();
            this.o1 = new System.Windows.Forms.Label();
            this.p2 = new System.Windows.Forms.Label();
            this.e1 = new System.Windows.Forms.Label();
            this.r1 = new System.Windows.Forms.Button();
            this.r2 = new System.Windows.Forms.Button();
            this.e2 = new System.Windows.Forms.Label();
            this.p4 = new System.Windows.Forms.Label();
            this.o2 = new System.Windows.Forms.Label();
            this.p3 = new System.Windows.Forms.Label();
            this.r3 = new System.Windows.Forms.Button();
            this.e3 = new System.Windows.Forms.Label();
            this.p6 = new System.Windows.Forms.Label();
            this.o3 = new System.Windows.Forms.Label();
            this.p5 = new System.Windows.Forms.Label();
            this.r4 = new System.Windows.Forms.Button();
            this.e4 = new System.Windows.Forms.Label();
            this.p8 = new System.Windows.Forms.Label();
            this.o4 = new System.Windows.Forms.Label();
            this.p7 = new System.Windows.Forms.Label();
            this.r5 = new System.Windows.Forms.Button();
            this.e5 = new System.Windows.Forms.Label();
            this.p10 = new System.Windows.Forms.Label();
            this.o5 = new System.Windows.Forms.Label();
            this.p9 = new System.Windows.Forms.Label();
            this.abandon = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // n1
            // 
            this.n1.BackColor = System.Drawing.Color.Teal;
            this.n1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.n1.ForeColor = System.Drawing.Color.White;
            this.n1.Location = new System.Drawing.Point(12, 42);
            this.n1.Name = "n1";
            this.n1.Size = new System.Drawing.Size(75, 23);
            this.n1.TabIndex = 0;
            this.n1.Text = "n1";
            this.n1.UseVisualStyleBackColor = false;
            this.n1.Click += new System.EventHandler(this.number_Click);
            // 
            // n2
            // 
            this.n2.BackColor = System.Drawing.Color.Teal;
            this.n2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.n2.ForeColor = System.Drawing.Color.White;
            this.n2.Location = new System.Drawing.Point(93, 42);
            this.n2.Name = "n2";
            this.n2.Size = new System.Drawing.Size(75, 23);
            this.n2.TabIndex = 1;
            this.n2.Text = "n2";
            this.n2.UseVisualStyleBackColor = false;
            this.n2.Click += new System.EventHandler(this.number_Click);
            // 
            // n3
            // 
            this.n3.BackColor = System.Drawing.Color.Teal;
            this.n3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.n3.ForeColor = System.Drawing.Color.White;
            this.n3.Location = new System.Drawing.Point(174, 42);
            this.n3.Name = "n3";
            this.n3.Size = new System.Drawing.Size(75, 23);
            this.n3.TabIndex = 2;
            this.n3.Text = "n3";
            this.n3.UseVisualStyleBackColor = false;
            this.n3.Click += new System.EventHandler(this.number_Click);
            // 
            // n4
            // 
            this.n4.BackColor = System.Drawing.Color.Teal;
            this.n4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.n4.ForeColor = System.Drawing.Color.White;
            this.n4.Location = new System.Drawing.Point(255, 42);
            this.n4.Name = "n4";
            this.n4.Size = new System.Drawing.Size(75, 23);
            this.n4.TabIndex = 3;
            this.n4.Text = "n4";
            this.n4.UseVisualStyleBackColor = false;
            this.n4.Click += new System.EventHandler(this.number_Click);
            // 
            // n5
            // 
            this.n5.BackColor = System.Drawing.Color.Teal;
            this.n5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.n5.ForeColor = System.Drawing.Color.White;
            this.n5.Location = new System.Drawing.Point(336, 42);
            this.n5.Name = "n5";
            this.n5.Size = new System.Drawing.Size(75, 23);
            this.n5.TabIndex = 4;
            this.n5.Text = "n5";
            this.n5.UseVisualStyleBackColor = false;
            this.n5.Click += new System.EventHandler(this.number_Click);
            // 
            // n6
            // 
            this.n6.BackColor = System.Drawing.Color.Teal;
            this.n6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.n6.ForeColor = System.Drawing.Color.White;
            this.n6.Location = new System.Drawing.Point(417, 42);
            this.n6.Name = "n6";
            this.n6.Size = new System.Drawing.Size(75, 23);
            this.n6.TabIndex = 5;
            this.n6.Text = "n6";
            this.n6.UseVisualStyleBackColor = false;
            this.n6.Click += new System.EventHandler(this.number_Click);
            // 
            // plus
            // 
            this.plus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.plus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.plus.Location = new System.Drawing.Point(417, 88);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(75, 23);
            this.plus.TabIndex = 6;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = false;
            this.plus.Click += new System.EventHandler(this.op_Click);
            // 
            // minus
            // 
            this.minus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.minus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.minus.Location = new System.Drawing.Point(417, 126);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(75, 23);
            this.minus.TabIndex = 7;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = false;
            this.minus.Click += new System.EventHandler(this.op_Click);
            // 
            // multi
            // 
            this.multi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.multi.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.multi.Location = new System.Drawing.Point(417, 167);
            this.multi.Name = "multi";
            this.multi.Size = new System.Drawing.Size(75, 23);
            this.multi.TabIndex = 8;
            this.multi.Text = "*";
            this.multi.UseVisualStyleBackColor = false;
            this.multi.Click += new System.EventHandler(this.op_Click);
            // 
            // div
            // 
            this.div.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.div.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.div.Location = new System.Drawing.Point(417, 206);
            this.div.Name = "div";
            this.div.Size = new System.Drawing.Size(75, 23);
            this.div.TabIndex = 9;
            this.div.Text = "/";
            this.div.UseVisualStyleBackColor = false;
            this.div.Click += new System.EventHandler(this.op_Click);
            // 
            // valider
            // 
            this.valider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.valider.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.valider.Location = new System.Drawing.Point(336, 238);
            this.valider.Name = "valider";
            this.valider.Size = new System.Drawing.Size(75, 23);
            this.valider.TabIndex = 10;
            this.valider.Text = "valider";
            this.valider.UseVisualStyleBackColor = false;
            this.valider.Click += new System.EventHandler(this.valider_Click);
            // 
            // effacer
            // 
            this.effacer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.effacer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.effacer.Location = new System.Drawing.Point(174, 238);
            this.effacer.Name = "effacer";
            this.effacer.Size = new System.Drawing.Size(75, 23);
            this.effacer.TabIndex = 11;
            this.effacer.Text = "effacer";
            this.effacer.UseVisualStyleBackColor = false;
            this.effacer.Click += new System.EventHandler(this.effacer_Click);
            // 
            // recommencer
            // 
            this.recommencer.BackColor = System.Drawing.Color.Yellow;
            this.recommencer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.recommencer.Location = new System.Drawing.Point(12, 238);
            this.recommencer.Name = "recommencer";
            this.recommencer.Size = new System.Drawing.Size(87, 23);
            this.recommencer.TabIndex = 12;
            this.recommencer.Text = "recommencer";
            this.recommencer.UseVisualStyleBackColor = false;
            this.recommencer.Click += new System.EventHandler(this.recommencer_Click);
            // 
            // resultat
            // 
            this.resultat.AutoSize = true;
            this.resultat.Location = new System.Drawing.Point(12, 9);
            this.resultat.Name = "resultat";
            this.resultat.Size = new System.Drawing.Size(25, 13);
            this.resultat.TabIndex = 13;
            this.resultat.Text = "000";
            // 
            // p1
            // 
            this.p1.AutoSize = true;
            this.p1.Location = new System.Drawing.Point(12, 19);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(27, 13);
            this.p1.TabIndex = 14;
            this.p1.Text = "vide";
            this.p1.Visible = false;
            // 
            // o1
            // 
            this.o1.AutoSize = true;
            this.o1.Location = new System.Drawing.Point(99, 19);
            this.o1.Name = "o1";
            this.o1.Size = new System.Drawing.Size(27, 13);
            this.o1.TabIndex = 15;
            this.o1.Text = "vide";
            this.o1.Visible = false;
            this.o1.TextChanged += new System.EventHandler(this.label_TextChanged);
            // 
            // p2
            // 
            this.p2.AutoSize = true;
            this.p2.Location = new System.Drawing.Point(190, 19);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(27, 13);
            this.p2.TabIndex = 16;
            this.p2.Text = "vide";
            this.p2.Visible = false;
            this.p2.TextChanged += new System.EventHandler(this.label_TextChanged);
            // 
            // e1
            // 
            this.e1.AutoSize = true;
            this.e1.Location = new System.Drawing.Point(249, 19);
            this.e1.Name = "e1";
            this.e1.Size = new System.Drawing.Size(13, 13);
            this.e1.TabIndex = 17;
            this.e1.Text = "=";
            this.e1.Visible = false;
            // 
            // r1
            // 
            this.r1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.r1.Location = new System.Drawing.Point(281, 14);
            this.r1.Name = "r1";
            this.r1.Size = new System.Drawing.Size(75, 23);
            this.r1.TabIndex = 18;
            this.r1.Text = "vide";
            this.r1.UseVisualStyleBackColor = true;
            this.r1.Visible = false;
            this.r1.TextChanged += new System.EventHandler(this.Result_TextChanged);
            this.r1.Click += new System.EventHandler(this.number_Click);
            // 
            // r2
            // 
            this.r2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.r2.Location = new System.Drawing.Point(281, 43);
            this.r2.Name = "r2";
            this.r2.Size = new System.Drawing.Size(75, 23);
            this.r2.TabIndex = 23;
            this.r2.Text = "vide";
            this.r2.UseVisualStyleBackColor = true;
            this.r2.Visible = false;
            this.r2.TextChanged += new System.EventHandler(this.Result_TextChanged);
            this.r2.Click += new System.EventHandler(this.number_Click);
            // 
            // e2
            // 
            this.e2.AutoSize = true;
            this.e2.Location = new System.Drawing.Point(249, 48);
            this.e2.Name = "e2";
            this.e2.Size = new System.Drawing.Size(13, 13);
            this.e2.TabIndex = 22;
            this.e2.Text = "=";
            this.e2.Visible = false;
            // 
            // p4
            // 
            this.p4.AutoSize = true;
            this.p4.Location = new System.Drawing.Point(190, 48);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(27, 13);
            this.p4.TabIndex = 21;
            this.p4.Text = "vide";
            this.p4.Visible = false;
            this.p4.TextChanged += new System.EventHandler(this.label_TextChanged);
            // 
            // o2
            // 
            this.o2.AutoSize = true;
            this.o2.Location = new System.Drawing.Point(99, 48);
            this.o2.Name = "o2";
            this.o2.Size = new System.Drawing.Size(27, 13);
            this.o2.TabIndex = 20;
            this.o2.Text = "vide";
            this.o2.Visible = false;
            this.o2.TextChanged += new System.EventHandler(this.label_TextChanged);
            // 
            // p3
            // 
            this.p3.AutoSize = true;
            this.p3.Location = new System.Drawing.Point(12, 48);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(27, 13);
            this.p3.TabIndex = 19;
            this.p3.Text = "vide";
            this.p3.Visible = false;
            // 
            // r3
            // 
            this.r3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.r3.Location = new System.Drawing.Point(281, 74);
            this.r3.Name = "r3";
            this.r3.Size = new System.Drawing.Size(75, 23);
            this.r3.TabIndex = 28;
            this.r3.Text = "vide";
            this.r3.UseVisualStyleBackColor = true;
            this.r3.Visible = false;
            this.r3.TextChanged += new System.EventHandler(this.Result_TextChanged);
            this.r3.Click += new System.EventHandler(this.number_Click);
            // 
            // e3
            // 
            this.e3.AutoSize = true;
            this.e3.Location = new System.Drawing.Point(249, 79);
            this.e3.Name = "e3";
            this.e3.Size = new System.Drawing.Size(13, 13);
            this.e3.TabIndex = 27;
            this.e3.Text = "=";
            this.e3.Visible = false;
            // 
            // p6
            // 
            this.p6.AutoSize = true;
            this.p6.Location = new System.Drawing.Point(190, 79);
            this.p6.Name = "p6";
            this.p6.Size = new System.Drawing.Size(27, 13);
            this.p6.TabIndex = 26;
            this.p6.Text = "vide";
            this.p6.Visible = false;
            this.p6.TextChanged += new System.EventHandler(this.label_TextChanged);
            // 
            // o3
            // 
            this.o3.AutoSize = true;
            this.o3.Location = new System.Drawing.Point(99, 79);
            this.o3.Name = "o3";
            this.o3.Size = new System.Drawing.Size(27, 13);
            this.o3.TabIndex = 25;
            this.o3.Text = "vide";
            this.o3.Visible = false;
            this.o3.TextChanged += new System.EventHandler(this.label_TextChanged);
            // 
            // p5
            // 
            this.p5.AutoSize = true;
            this.p5.Location = new System.Drawing.Point(12, 79);
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(27, 13);
            this.p5.TabIndex = 24;
            this.p5.Text = "vide";
            this.p5.Visible = false;
            // 
            // r4
            // 
            this.r4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.r4.Location = new System.Drawing.Point(281, 103);
            this.r4.Name = "r4";
            this.r4.Size = new System.Drawing.Size(75, 23);
            this.r4.TabIndex = 33;
            this.r4.Text = "vide";
            this.r4.UseVisualStyleBackColor = true;
            this.r4.Visible = false;
            this.r4.TextChanged += new System.EventHandler(this.Result_TextChanged);
            this.r4.Click += new System.EventHandler(this.number_Click);
            // 
            // e4
            // 
            this.e4.AutoSize = true;
            this.e4.Location = new System.Drawing.Point(249, 108);
            this.e4.Name = "e4";
            this.e4.Size = new System.Drawing.Size(13, 13);
            this.e4.TabIndex = 32;
            this.e4.Text = "=";
            this.e4.Visible = false;
            // 
            // p8
            // 
            this.p8.AutoSize = true;
            this.p8.Location = new System.Drawing.Point(190, 108);
            this.p8.Name = "p8";
            this.p8.Size = new System.Drawing.Size(27, 13);
            this.p8.TabIndex = 31;
            this.p8.Text = "vide";
            this.p8.Visible = false;
            this.p8.TextChanged += new System.EventHandler(this.label_TextChanged);
            // 
            // o4
            // 
            this.o4.AutoSize = true;
            this.o4.Location = new System.Drawing.Point(99, 108);
            this.o4.Name = "o4";
            this.o4.Size = new System.Drawing.Size(27, 13);
            this.o4.TabIndex = 30;
            this.o4.Text = "vide";
            this.o4.Visible = false;
            this.o4.TextChanged += new System.EventHandler(this.label_TextChanged);
            // 
            // p7
            // 
            this.p7.AutoSize = true;
            this.p7.Location = new System.Drawing.Point(12, 108);
            this.p7.Name = "p7";
            this.p7.Size = new System.Drawing.Size(27, 13);
            this.p7.TabIndex = 29;
            this.p7.Text = "vide";
            this.p7.Visible = false;
            // 
            // r5
            // 
            this.r5.Enabled = false;
            this.r5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.r5.Location = new System.Drawing.Point(281, 132);
            this.r5.Name = "r5";
            this.r5.Size = new System.Drawing.Size(75, 23);
            this.r5.TabIndex = 38;
            this.r5.Text = "vide";
            this.r5.UseVisualStyleBackColor = true;
            this.r5.Visible = false;
            this.r5.TextChanged += new System.EventHandler(this.Result_TextChanged);
            // 
            // e5
            // 
            this.e5.AutoSize = true;
            this.e5.Location = new System.Drawing.Point(249, 137);
            this.e5.Name = "e5";
            this.e5.Size = new System.Drawing.Size(13, 13);
            this.e5.TabIndex = 37;
            this.e5.Text = "=";
            this.e5.Visible = false;
            // 
            // p10
            // 
            this.p10.AutoSize = true;
            this.p10.Location = new System.Drawing.Point(190, 137);
            this.p10.Name = "p10";
            this.p10.Size = new System.Drawing.Size(27, 13);
            this.p10.TabIndex = 36;
            this.p10.Text = "vide";
            this.p10.Visible = false;
            this.p10.TextChanged += new System.EventHandler(this.label_TextChanged);
            // 
            // o5
            // 
            this.o5.AutoSize = true;
            this.o5.Location = new System.Drawing.Point(99, 137);
            this.o5.Name = "o5";
            this.o5.Size = new System.Drawing.Size(27, 13);
            this.o5.TabIndex = 35;
            this.o5.Text = "vide";
            this.o5.Visible = false;
            this.o5.TextChanged += new System.EventHandler(this.label_TextChanged);
            // 
            // p9
            // 
            this.p9.AutoSize = true;
            this.p9.Location = new System.Drawing.Point(12, 137);
            this.p9.Name = "p9";
            this.p9.Size = new System.Drawing.Size(27, 13);
            this.p9.TabIndex = 34;
            this.p9.Text = "vide";
            this.p9.Visible = false;
            // 
            // abandon
            // 
            this.abandon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.abandon.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.abandon.Location = new System.Drawing.Point(417, 9);
            this.abandon.Name = "abandon";
            this.abandon.Size = new System.Drawing.Size(75, 23);
            this.abandon.TabIndex = 39;
            this.abandon.Text = "abandon";
            this.abandon.UseVisualStyleBackColor = false;
            this.abandon.Click += new System.EventHandler(this.abandon_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.r5);
            this.groupBox1.Controls.Add(this.p1);
            this.groupBox1.Controls.Add(this.o1);
            this.groupBox1.Controls.Add(this.e5);
            this.groupBox1.Controls.Add(this.p2);
            this.groupBox1.Controls.Add(this.p10);
            this.groupBox1.Controls.Add(this.e1);
            this.groupBox1.Controls.Add(this.o5);
            this.groupBox1.Controls.Add(this.r1);
            this.groupBox1.Controls.Add(this.p9);
            this.groupBox1.Controls.Add(this.p3);
            this.groupBox1.Controls.Add(this.r4);
            this.groupBox1.Controls.Add(this.o2);
            this.groupBox1.Controls.Add(this.e4);
            this.groupBox1.Controls.Add(this.p4);
            this.groupBox1.Controls.Add(this.p8);
            this.groupBox1.Controls.Add(this.e2);
            this.groupBox1.Controls.Add(this.o4);
            this.groupBox1.Controls.Add(this.r2);
            this.groupBox1.Controls.Add(this.p7);
            this.groupBox1.Controls.Add(this.p5);
            this.groupBox1.Controls.Add(this.r3);
            this.groupBox1.Controls.Add(this.o3);
            this.groupBox1.Controls.Add(this.e3);
            this.groupBox1.Controls.Add(this.p6);
            this.groupBox1.Location = new System.Drawing.Point(12, 68);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 161);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(503, 271);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.abandon);
            this.Controls.Add(this.resultat);
            this.Controls.Add(this.recommencer);
            this.Controls.Add(this.effacer);
            this.Controls.Add(this.valider);
            this.Controls.Add(this.div);
            this.Controls.Add(this.multi);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.n6);
            this.Controls.Add(this.n5);
            this.Controls.Add(this.n4);
            this.Controls.Add(this.n3);
            this.Controls.Add(this.n2);
            this.Controls.Add(this.n1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button n1;
        private System.Windows.Forms.Button n2;
        private System.Windows.Forms.Button n3;
        private System.Windows.Forms.Button n4;
        private System.Windows.Forms.Button n5;
        private System.Windows.Forms.Button n6;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button multi;
        private System.Windows.Forms.Button div;
        private System.Windows.Forms.Button valider;
        private System.Windows.Forms.Button effacer;
        private System.Windows.Forms.Button recommencer;
        private System.Windows.Forms.Label resultat;
        private System.Windows.Forms.Label p1;
        private System.Windows.Forms.Label o1;
        private System.Windows.Forms.Label p2;
        private System.Windows.Forms.Label e1;
        private System.Windows.Forms.Button r1;
        private System.Windows.Forms.Button r2;
        private System.Windows.Forms.Label e2;
        private System.Windows.Forms.Label p4;
        private System.Windows.Forms.Label o2;
        private System.Windows.Forms.Label p3;
        private System.Windows.Forms.Button r3;
        private System.Windows.Forms.Label e3;
        private System.Windows.Forms.Label p6;
        private System.Windows.Forms.Label o3;
        private System.Windows.Forms.Label p5;
        private System.Windows.Forms.Button r4;
        private System.Windows.Forms.Label e4;
        private System.Windows.Forms.Label p8;
        private System.Windows.Forms.Label o4;
        private System.Windows.Forms.Label p7;
        private System.Windows.Forms.Button r5;
        private System.Windows.Forms.Label e5;
        private System.Windows.Forms.Label p10;
        private System.Windows.Forms.Label o5;
        private System.Windows.Forms.Label p9;
        private System.Windows.Forms.Button abandon;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

