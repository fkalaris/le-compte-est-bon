﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompteEstBon
{
    public partial class Form1 : Form
    {
        Game partie;
        Calcul calcul;
        Checks verification;
        Board terrain;
        public Form1()
        {
            partie = new Game();
            calcul = new Calcul();
            verification = new Checks();
            terrain = partie.Getboard();
            InitializeComponent();

        }
        public Form1(int nbToFind, int[] numbers)
        {
            partie = new Game(nbToFind,numbers);
            calcul = new Calcul();
            verification = new Checks();
            terrain = partie.Getboard();
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            this.resultat.Text = partie.Getresult().ToString();
            this.n1.Text = partie.Getn1().ToString();
            this.n2.Text = partie.Getn2().ToString();
            this.n3.Text = partie.Getn3().ToString();
            this.n4.Text = partie.Getn4().ToString();
            this.n5.Text = partie.Getn5().ToString();
            this.n6.Text = partie.Getn6().ToString();
        }

        private void number_Click(object sender, EventArgs e)
        {
            
            Control NumberPressed = ((Control)sender);                                                              //défini le control préssé             
            //si le dernier nombre n'est pas le dernier possible
            if (terrain.getLastNb() != 10)
            {
                int i = terrain.addNumber();                                                                        //incrémente la position et récupère la position dans lequel mettre le nombre
                Control labelToModif = this.Controls.Find("p" + i.ToString(), true).FirstOrDefault() as Control;    //recupère le label à modifier en fonction du i
                labelToModif.Text = NumberPressed.Text;                                                             //donne la valeur du nombre appuyé au label a modifier
                labelToModif.Visible = true;                                                                        //affiche le label
                NumberPressed.Enabled = false;                                                                      //desactive le boutton appuyé

                //si le nombre appuyé est un des 6 nombre originaux (est un control qui commence par 'n' et pas 'r'
                if (NumberPressed.Name.Contains('n') == true)
                {
                    partie.SetCorresp(labelToModif.Name, NumberPressed.Name);                                       //enregistre le label dans lequel le nombre à été mis
                }
            }
        }

        private void op_Click(object sender, EventArgs e)
        {
            Control OperatorPressed = ((Control)sender);                                                            //défini le control préssé
            //si la dernière opération n'est pas la dernière possible
            if (terrain.getLastOp() != 5)
            {
                int i = terrain.addOp();                                                                            //incrémente la position et récupère la position dans lequel mettre l'opération
                Control labelToModif  = this.Controls.Find("o" + i.ToString(), true).FirstOrDefault() as Control;   //recupère le label à modifier en fonction du i
                labelToModif.Text = OperatorPressed.Text;                                                           //donne la valeur de l'opération appuyé au label a modifier
                labelToModif.Visible = true;                                                                        //affiche le label
            }
        }

        //evènement déclenché au changement de texte dans un label
        private void label_TextChanged(object sender, EventArgs e)
        {
            Control labelChanged = ((Control)sender);                                                                                   //défini le control dont le texte à changé
            Control labelOperator;
            Control labelnb1;
            Control labelnb2;
            Control ButtonResultat;

            int chiffre = Convert.ToInt32(labelChanged.Name.Substring(1));  //récupère le numero du label qui à changé
            string lettre = labelChanged.Name.Substring(0, 1);              //récupère la lettre du label qui à changé       

            //si le label contient un numéro (la première lettre est un p)
            if (lettre == "p")
            {
                //définit tous les controls nécessaire pour le calcul (l'opération, le premier nombre, le second nombre, le resultat)
                labelOperator = this.Controls.Find("o" + (chiffre / 2).ToString(), true).FirstOrDefault() as Control;
                labelnb1 = this.Controls.Find("p" + (chiffre - 1).ToString(), true).FirstOrDefault() as Control;
                labelnb2 = this.Controls.Find("p" + chiffre.ToString(), true).FirstOrDefault() as Control;
                ButtonResultat = this.Controls.Find("r" + (chiffre / 2).ToString(), true).FirstOrDefault() as Control;
            }
            //si le label contient une opération (la première lettre est un o)
            else
            {
                //définit tous les controls nécessaire pour le calcul (l'opération, le premier nombre, le second nombre, le resultat)
                labelOperator = this.Controls.Find("o" + chiffre.ToString(), true).FirstOrDefault() as Control;
                labelnb1 = this.Controls.Find("p" + ((chiffre * 2) - 1).ToString(), true).FirstOrDefault() as Control;
                labelnb2 = this.Controls.Find("p" + (chiffre * 2).ToString(), true).FirstOrDefault() as Control;
                ButtonResultat = this.Controls.Find("r" + chiffre.ToString(), true).FirstOrDefault() as Control;
            }
            //si les deux nombres et l'opération sont remseigné
            if (labelOperator.Text != "vide" && labelnb1.Text != "vide" && labelnb2.Text != "vide")
            {
                //si la vérification du calcul retourne true
                if (verification.CheckCalcul(Convert.ToInt32(labelnb1.Text), Convert.ToInt32(labelnb2.Text), labelOperator.Text) == true)
                {
                    ButtonResultat.Text = calcul.calculation(Convert.ToInt32(labelnb1.Text), Convert.ToInt32(labelnb2.Text), labelOperator.Text).ToString();    //fait le calcul et affiche le resultat dans le boutton
                }
                else
                {
                    ButtonResultat.Text = "vide";   
                }

            }
            else
            {
                ButtonResultat.Text = "vide";
            }
        }


        //évènement déclenché au click sur le boutton éffacer
        private void effacer_Click(object sender, EventArgs e)
        {
            //si il y a déjà eu un mouvement
            if (terrain.getLastMove() != "")
            {
                string NomLabelLastMove = terrain.removeLastMove();                                             //recupère le nom du control du dernier mouvement et le supprime dans le terrain
                string NomButtonCorrespondant = partie.GetCorresp(NomLabelLastMove);                                //récupère le nom du boutton correspondant au label
                Control LabelLastMove = this.Controls.Find(NomLabelLastMove, true).FirstOrDefault() as Control;     //defini le label du dernier mouvement

                LabelLastMove.Text = "vide";
                LabelLastMove.Visible = false;
                //si le nom n'est pas vide
                if (NomButtonCorrespondant != "")
                {
                    Control ButtonCorrespondant = this.Controls.Find(NomButtonCorrespondant, true).FirstOrDefault() as Control; //defini le boutton correspondant au label du dernier mouvement
                    ButtonCorrespondant.Enabled = true;
                }
            }  
        }

        //evenement déclenché si la valeur d'un resultat change
        private void Result_TextChanged(object sender, EventArgs e)
        {
            Control ButtonResultat = ((Control)sender);
            Control LabelEgal = this.Controls.Find((ButtonResultat.Name).Replace('r','e'), true).FirstOrDefault() as Control;
            //si le resultat est vide cache les controls sinon les affiches
            if (ButtonResultat.Text == "vide")
            {
                ButtonResultat.Enabled = false;
                ButtonResultat.Visible = false;
                LabelEgal.Visible = false;
            }
            else
            {
                ButtonResultat.Enabled = true;
                ButtonResultat.Visible = true;
                LabelEgal.Visible = true;
            }
        }

        private void recommencer_Click(object sender, EventArgs e)
        {
            //regénère un formulaire en gardant le valeur charché puis détruit le formulaire actuel
            Form1 NewForm = new Form1(partie.Getresult(),partie.GetNumbers());
            NewForm.Show();
            this.Dispose(false);
        }

        private void abandon_Click(object sender, EventArgs e)
        {
            //regénère un formulaire puis  détruit le formulaire actuel
            Form1 NewForm = new Form1();
            NewForm.Show();
            this.Dispose(false);
        }

        private void valider_Click(object sender, EventArgs e)
        {
            //si il y a deja eu une opération
            if (terrain.getLastOp() != 0)
            {
                Control lastResultat = this.Controls.Find("r" + terrain.getLastOp(), true).FirstOrDefault() as Control;
                //si le dernier resultat n'est pas vide
                if (lastResultat.Text != "vide")
                {
                    Fin NewForm;
                    //si le resultat est egal à la valeur trouvé
                    if (lastResultat.Text == resultat.Text)
                    {
                        NewForm = new Fin("Vous avez gagné");
                    }
                    else
                    {
                        NewForm = new Fin("Vous avez perdu");
                    }
                    NewForm.Show();
                    this.Dispose(false);
                }
            }
        }
    }
}
