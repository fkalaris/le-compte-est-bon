﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CompteEstBon
{
    class Calcul
    {
        //effectue le calcul entre les deux nombres
        //n1 : premier nombre
        //n2 : second nombre
        //op : opération
        public int calculation(int n1, int n2, string op)
        {
            DataTable dt = new DataTable();

            //concatene toutes les valeurs de l'opération dans un string
            String math = (n1 + " " + op + " " + n2).ToString();

            //fait l'opération et retourne le resultat
            return Convert.ToInt32(dt.Compute(math, ""));
        }   
    }
}
