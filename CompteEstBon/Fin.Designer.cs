﻿namespace CompteEstBon
{
    partial class Fin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.game = new System.Windows.Forms.Button();
            this.txt = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // game
            // 
            this.game.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.game.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.game.Location = new System.Drawing.Point(27, 30);
            this.game.Name = "game";
            this.game.Size = new System.Drawing.Size(96, 29);
            this.game.TabIndex = 11;
            this.game.Text = "Nouvelle partie";
            this.game.UseVisualStyleBackColor = false;
            this.game.Click += new System.EventHandler(this.game_Click);
            // 
            // txt
            // 
            this.txt.AutoSize = true;
            this.txt.Location = new System.Drawing.Point(24, 14);
            this.txt.Name = "txt";
            this.txt.Size = new System.Drawing.Size(27, 13);
            this.txt.TabIndex = 14;
            this.txt.Text = "vide";
            // 
            // Fin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(141, 68);
            this.Controls.Add(this.txt);
            this.Controls.Add(this.game);
            this.Name = "Fin";
            this.Text = "Fin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button game;
        private System.Windows.Forms.Label txt;
    }
}