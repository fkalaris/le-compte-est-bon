﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompteEstBon
{
    class Board
    {
        private string lastMove;
        private int lastOp;
        private int lastNb;

        public Board()
        {
            setLastMove("");
            setLastOp(0);
            setLastNb(0);
        }

        public void setLastMove(string lm)
        {
            lastMove = lm;
        }
        public void setLastOp(int op)
        {
            lastOp = op;
        }
        public void setLastNb(int nb)
        {
            lastNb = nb;
        }
        public string getLastMove()
        {
            return lastMove;
        }
        public int getLastOp()
        {
            return lastOp;
        }
        public int getLastNb()
        {
            return lastNb;
        }

        //incremente le nombre de numéro cliqué et indique le dernier mouvement est un nombre
        public int addNumber()
        {
            setLastNb(getLastNb() + 1);
            setLastMove("p");
            return getLastNb();
        }

        //incremente le nombre d'opération cliqué et indique le dernier mouvement est une opération
        public int addOp()
        {
            setLastOp(getLastOp() + 1);
            setLastMove("o");
            return getLastOp();
        }

        //annule la dernière opération de l'utilisateur
        public string removeLastMove()
        {
            string retour = "";
            //decremente le dernier nombre ou la dernière opération
            switch (lastMove)
            {
                case "p":
                    retour = lastMove + lastNb;
                    lastNb = lastNb - 1;
                    break;

                case "o":
                    retour = lastMove + lastOp;
                    lastOp = lastOp - 1;
                    break;

                default:
                    break;
            }
            if (lastNb == 0 && lastOp == 0)
            {
                lastMove = "";
            }
            else if (lastNb >= (lastOp*2))
            {
                lastMove = "p";
            }
            else
            {
                lastMove = "o";
            }

            return retour;
        }
    }
}
