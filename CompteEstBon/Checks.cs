﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompteEstBon
{
    class Checks
    {
        //vérifie que l'opération "op" donne un resultat entier positif
        public bool CheckCalcul(int n1, int n2, string op)
        {
            bool result = false;
            //switch en fonction de l'opération
            switch (op)
            {
                case "+":
                    result = true;
                    break;

                case "-":
                    //si le premier nombre est plus grand que le second
                    if(n1 > n2)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    break;

                case "*":
                    result = true;
                    break;

                case "/":
                    //si la division n'a pas de reste
                    if (n1 % n2 == 0)
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                    break;

                default:
                    result = false; 
                    break;
            }
            return result;
        }
    }
}
