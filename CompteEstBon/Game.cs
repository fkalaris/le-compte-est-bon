﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;


namespace CompteEstBon
{
    class Game
    {
        private int result;
        Board terrain;
        //liste das nombres aléatoire
        int[] numbers = new int[6];

        //tableau de correspondance pour savoir dans quel label le nombre du boutton à été placé
        string[] controlsToNumber = new string[6];

        //constructeur qui genere les nombres de la partie et met en place le terrain
        public Game()
        {
            Random rnd = new Random();
            result = rnd.Next(100, 1000);
            Bag sac = new Bag();
            numbers[0] = sac.picknumber();
            numbers[1] = sac.picknumber();
            numbers[2] = sac.picknumber();
            numbers[3] = sac.picknumber();
            numbers[4] = sac.picknumber();
            numbers[5] = sac.picknumber();

            controlsToNumber[0] = "";
            controlsToNumber[1] = "";
            controlsToNumber[2] = "";
            controlsToNumber[3] = "";
            controlsToNumber[4] = "";
            controlsToNumber[5] = "";
            terrain = new Board();
        }

        //constructeur qui prend en parametre le resultat recherché
        public Game(int nbToFind, int[] numbers)
        {
            result = nbToFind;
            Bag sac = new Bag();
            this.numbers = numbers;

            controlsToNumber[0] = "";
            controlsToNumber[1] = "";
            controlsToNumber[2] = "";
            controlsToNumber[3] = "";
            controlsToNumber[4] = "";
            controlsToNumber[5] = "";
            terrain = new Board();
        }
        public int[] GetNumbers()
        {
            return numbers;
        }
        public Board Getboard()
        {
            return terrain;
        }

        public int Getn1()
        {
            return numbers[0];
        }

        public int Getn2()
        {
            return numbers[1];
        }
        public int Getn3()
        {
            return numbers[2];
        }
        public int Getn4()
        {
            return numbers[3];
        }
        public int Getn5()
        {
            return numbers[4];
        }
        public int Getn6()
        {
            return numbers[5];
        }
  
        public string GetContn1()
        {
            return controlsToNumber[0];
        }
        public string GetContn2()
        {
            return controlsToNumber[1];
        }
        public string GetContn3()
        {
            return controlsToNumber[2];
        }
        public string GetContn4()
        {
            return controlsToNumber[3];
        }
        public string GetContn5()
        {
            return controlsToNumber[4];
        }
        public string GetContn6()
        {
            return controlsToNumber[5];
        }

        public void SetContn1(string cont)
        {
            controlsToNumber[0] = cont;
        }
        public void SetContn2(string cont)
        {
            controlsToNumber[1] = cont;
        }
        public void SetContn3(string cont)
        {
            controlsToNumber[2] = cont;
        }
        public void SetContn4(string cont)
        {
            controlsToNumber[3] = cont;
        }
        public void SetContn5(string cont)
        {
            controlsToNumber[4] = cont;
        }
        public void SetContn6(string cont)
        {
            controlsToNumber[5] = cont;
        }

        //défini la correspondence entre le numbre sélectionné et sa position sur le terrain
        //nomCont : nom du control dans lequel le nombre à été mis
        //nomButton : nom du boutton cliqué
        public void SetCorresp(string nomCont, string nomButton)
        {
            //recupère le numero du boutton
            int i = Convert.ToInt32(nomButton.Remove(0, 1));
            controlsToNumber[i - 1] = nomCont;
        }

        //récupère le numéro correspondant a la position sur le terrain
        public string GetCorresp(string cont)
        {
            //cherche dans la list de correspondance si le control y est
            for(int i = 0; i < 6; i++)
            {
                if(cont == controlsToNumber[i])
                {
                    return "n" + (i+1);
                }
            }
            return "";
        }
        public int Getresult()
        {
            return result;
        }
    }
}
